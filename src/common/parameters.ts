import { envName } from '@swypeless/core';

export function emailParameterId(id: string): string {
  return `/email/${envName()}/${id}`;
}
