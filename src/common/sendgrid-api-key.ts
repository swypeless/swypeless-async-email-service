import { envName, SecretsManagerProvider } from '@swypeless/core';

const CACHE_MAX = 100;
const CACHE_TTL = 300;

export type SendgridSecret = {
  secret: string,
};

const secretsManager = new SecretsManagerProvider({
  max: CACHE_MAX,
  ttl: CACHE_TTL,
});

function sendgridSecretId(key: string): string {
  return `sendgrid/${envName()}/${key}`;
}

const SENDGRID_API_KEY_SECRET_ID = sendgridSecretId('api-key');

export async function getSendgridApiKey(): Promise<string> {
  try {
    const sendgridApiKey = await secretsManager
      .getSecret<SendgridSecret>(SENDGRID_API_KEY_SECRET_ID);
    return sendgridApiKey.secret;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('ERROR: fetching api key secret');
    throw error;
  }
}
