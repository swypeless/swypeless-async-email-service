import { INTERNAL_BASE_URL } from '@swypeless/async-microservice-core';
import { MERCHANT_OBJECT, RemoteMerchantProvider, EmailResource } from '@swypeless/core';

const merchantProvider = new RemoteMerchantProvider(INTERNAL_BASE_URL);

export async function fetchResource(resource: EmailResource): Promise<any> {
  switch (resource.object) {
    case MERCHANT_OBJECT:
      return merchantProvider.getByPrimaryKey(resource.id);
    default:
      break;
  }
  throw new Error(`Unsupported resource object type: ${resource.object}`);
}
