import client from '@sendgrid/client';
import { ClientRequest } from '@sendgrid/client/src/request';
import { AddContactResponseData, Contact } from '../types';
import { getSendgridApiKey } from '/opt/nodejs/sendgrid-api-key';

export async function addContact(contact: Contact, listIds: string[]): Promise<string> {
  // set api key on each call to keep real-time api keys within bounds of secrets manager cache
  const apiKey = await getSendgridApiKey();
  client.setApiKey(apiKey);
  // create data object
  const data = {
    list_ids: listIds,
    contacts: [contact],
  };
  // create request object
  const request: ClientRequest = {
    url: '/v3/marketing/contacts',
    method: 'PUT',
    body: data,
  };
  // send request
  const [,resultData]: [any, AddContactResponseData] = await client.request(request);
  // return
  return resultData.job_id;
}
