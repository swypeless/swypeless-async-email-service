import { Merchant, MERCHANT_OBJECT } from '@swypeless/core';
import { Contact } from '../types';

export function getContactData(resource: any, object: string): Contact {
  let contact: Contact;
  switch (object) {
    case MERCHANT_OBJECT:
      contact = getMerchantContactData(resource as Merchant);
      break;
    default:
      throw new Error(`Unsupported resource for contact data: ${object}`);
  }
  return trimContactData(contact);
}

function getMerchantContactData(merchant: Merchant): Contact {
  return {
    email: merchant.email,
    first_name: undefinedify(merchant.individual.first_name),
    last_name: undefinedify(merchant.individual.last_name),
    address_line_1: undefinedify(merchant.business_profile.support_address.line1),
    address_line_2: undefinedify(merchant.business_profile.support_address.line2),
    city: undefinedify(merchant.business_profile.support_address.city),
    country: undefinedify(merchant.business_profile.support_address.country),
    postal_code: undefinedify(merchant.business_profile.support_address.postal_code),
    state_province_region: undefinedify(merchant.business_profile.support_address.state),
  };
}

// trim contact data to sendgrid limits
function trimContactData(contact: Contact): Contact {
  return {
    ...contact,
    address_line_1: trimData(contact.address_line_1, 100),
    address_line_2: trimData(contact.address_line_2, 100),
    alternate_emails: contact.alternate_emails && contact.alternate_emails.slice(0, 5),
    city: trimData(contact.city, 60),
    country: trimData(contact.country, 50),
    email: trimData(contact.email, 254),
    first_name: trimData(contact.first_name, 50),
    last_name: trimData(contact.last_name, 50),
    state_province_region: trimData(contact.state_province_region, 50),
  };
}

// overloaded trim data
function trimData(value: string, length: number): string;
function trimData(value: string | undefined, length: number): string | undefined;
function trimData(value: string | undefined, length: number): string | undefined {
  return value && value.substring(0, length);
}

function undefinedify(value: string | null | undefined): string | undefined {
  return value === null ? undefined : value;
}
