import { SSMParametersProvider } from '@swypeless/core';
import {
  CACHE_MAX,
  CACHE_TTL,
} from '../constants';
import { emailParameterId } from '/opt/nodejs/parameters';

const parametersProvider = new SSMParametersProvider({
  max: CACHE_MAX,
  ttl: CACHE_TTL,
});

const LIST_IDS_PARAMETER_ID = emailParameterId('list-ids');

export async function getListIds(lists: string[]): Promise<string[]> {
  const parameterValue = await parametersProvider.getParameter(LIST_IDS_PARAMETER_ID);
  const listIds = JSON.parse(parameterValue) as Record<string, string>;
  return lists
    .map((listName) => listIds[listName])
    .filter(Boolean);
}
