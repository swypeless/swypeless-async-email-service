export const CACHE_MAX = 100;
export const CACHE_TTL = 300;
export const IS_PROD = process.env.SERVICE_ENV === 'production';
