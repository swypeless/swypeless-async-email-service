import { SQSHandler } from '@swypeless/async-microservice-core';
import { Event, SubscribeEmailEvent } from '@swypeless/core';
import { getContactData } from './utils/contacts';
import { getListIds } from './utils/list-ids';
import { addContact } from './utils/sendgrid';
import { fetchResource } from '/opt/nodejs/resources';

const eventHandler = async (event: Event<SubscribeEmailEvent>) => {
  const subscribeData = event.data.object;
  const listIds = await getListIds(subscribeData.lists);
  const resource = await fetchResource(subscribeData.resource);
  const contact = getContactData(resource, subscribeData.resource.object);
  await addContact(contact, listIds);
};

export default SQSHandler(eventHandler);
