export interface Contact {
  address_line_1?: string;
  address_line_2?: string;
  alternate_emails?: string[];
  city?: string;
  country?: string;
  email: string;
  first_name?: string;
  last_name?: string;
  postal_code?: string;
  state_province_region?: string;
  custom_fields?: Record<string, string | number | boolean>;
}

export interface AddContactResponseData {
  job_id: string;
}
