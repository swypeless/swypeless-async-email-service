import { MerchantBusinessProfile } from '@swypeless/core';

export enum EmailTarget {
  Merchant = 'merchant',
  SaleCustomer = 'sale_customer',
}

export interface EmailData {
  email: string;
  name?: string;
}

export interface EmailTemplateConfig {
  name: string;
  sendgrid_template_id: string;
  target: EmailTarget;
  from: EmailData;
  required_resources?: string[];
}

// object sent to sendgrid for templating
export interface EmailMerchant {
  email: string;
  first_name: string | null;
  last_name: string | null;
  full_name: string | null;
  default_currency: string;
  country: string;
  business_profile: MerchantBusinessProfile;
}
