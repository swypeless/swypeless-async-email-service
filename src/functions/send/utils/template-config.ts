import { SSMParametersProvider } from '@swypeless/core';
import {
  CACHE_MAX,
  CACHE_TTL,
} from '../constants';
import { EmailTemplateConfig } from '../types';
import { emailParameterId } from '/opt/nodejs/parameters';

const parametersProvider = new SSMParametersProvider({
  max: CACHE_MAX,
  ttl: CACHE_TTL,
});

export async function getTemplateConfig(name: string): Promise<EmailTemplateConfig> {
  const parameterId = templateConfigParameterId(name);
  const parameterValue = await parametersProvider.getParameter(parameterId);
  return JSON.parse(parameterValue) as EmailTemplateConfig;
}

function templateConfigParameterId(name: string): string {
  return emailParameterId(`template-configs/${name}`);
}
