/* eslint-disable @typescript-eslint/naming-convention */
import { Merchant, MERCHANT_OBJECT } from '@swypeless/core';
import { EmailMerchant } from '../types';

export function getDynamicTemplateData(
  requiredResources: string[],
  resources: Record<string, any>,
): Record<string, any> {
  return requiredResources
    .reduce((prev, resource) => ({
      ...prev,
      [resource]: getResourceData(resource, resources),
    }), {});
}

function getResourceData(resource: string, resources: Record<string, any>): any {
  switch (resource) {
    case MERCHANT_OBJECT:
      return getMerchantResourceData(resources[MERCHANT_OBJECT] as Merchant);
    default:
      break;
  }
  throw new Error(`Unsupported resource for resource data: ${resource}`);
}

function getMerchantResourceData(merchant: Merchant): EmailMerchant {
  const {
    email,
    default_currency,
    country,
    business_profile,
  } = merchant;
  const { first_name, last_name } = merchant.individual;
  const full_name = `${first_name || ''} ${last_name || ''}`.trim();
  return {
    email,
    first_name,
    last_name,
    full_name,
    default_currency,
    country,
    business_profile,
  };
}
