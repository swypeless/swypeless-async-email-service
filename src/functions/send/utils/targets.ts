import { Merchant, MERCHANT_OBJECT } from '@swypeless/core';
import { EmailData, EmailTarget } from '../types';

export function getEmailTarget(target: EmailTarget, resources: Record<string, any>): EmailData {
  switch (target) {
    case EmailTarget.Merchant:
      return getMerchantEmailTarget(resources[MERCHANT_OBJECT] as Merchant);
    default:
      break;
  }
  throw new Error(`Unsupported email target: ${target}`);
}

function getMerchantEmailTarget(merchant: Merchant): EmailData {
  let name: string | undefined;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { first_name, last_name } = merchant.individual;
  if (first_name || last_name) {
    name = `${first_name || ''} ${last_name || ''}`.trim();
  }
  return {
    email: merchant.email,
    name,
  };
}
