import sendgrid, { MailDataRequired } from '@sendgrid/mail';
import { getSendgridApiKey } from '/opt/nodejs/sendgrid-api-key';

export async function sendMail(data: MailDataRequired): Promise<void> {
  // set api key on each call to keep real-time api keys within bounds of secrets manager cache
  const apiKey = await getSendgridApiKey();
  sendgrid.setApiKey(apiKey);
  // send
  await sendgrid.send(data);
}
