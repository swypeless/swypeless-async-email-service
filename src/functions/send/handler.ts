import { Event, SendEmailEvent } from '@swypeless/core';
import { SQSHandler } from '@swypeless/async-microservice-core';
import { getTemplateConfig } from './utils/template-config';
import { IS_PROD } from './constants';
import { sendMail } from './utils/sendgrid';
import { getEmailTarget } from './utils/targets';
import { getDynamicTemplateData } from './utils/dynamic-template-data';
import { fetchResource } from '/opt/nodejs/resources';

const eventHandler = async (event: Event<SendEmailEvent>) => {
  const sendData = event.data.object;

  // get template config
  const templateConfig = await getTemplateConfig(sendData.template_name);
  const requiredResources = templateConfig.required_resources || [];

  // verify required resources
  const hasRequiredResources = requiredResources
    .every((resource) => sendData.resources
      .find((sendMailResource) => sendMailResource.object === resource));
  if (!hasRequiredResources) {
    throw new Error(`Missing required resources: ${(templateConfig.required_resources || []).join(', ')}`);
  }

  // fetch required resources
  const resourceObjects = await Promise.all(requiredResources
    .map((resource) => sendData.resources
      .find((sendDataResource) => sendDataResource.object === resource))
    .map((resource) => resource && fetchResource(resource))
    .filter((promise) => promise !== undefined));
  const resources = requiredResources
    .reduce((prev, resource, index) => ({ ...prev, [resource]: resourceObjects[index] }), {});

  // get email target
  const target = getEmailTarget(templateConfig.target, resources);

  // get dynamic template data
  const dynamicTemplateData = getDynamicTemplateData(requiredResources, resources);

  // create message data
  const message = {
    templateId: templateConfig.sendgrid_template_id,
    personalizations: [
      {
        to: [target],
        dynamic_template_data: dynamicTemplateData,
      },
    ],
    from: templateConfig.from,
    replyTo: templateConfig.from,
    mailSettings: {
      bypassListManagement: {
        enable: false,
      },
      footer: {
        enable: false,
      },
      sandboxMode: {
        enable: !IS_PROD,
      },
    },
    trackingSettings: {
      clickTracking: {
        enable: true,
        enableText: false,
      },
      openTracking: {
        enable: true,
        substitutionTag: '%open-track%',
      },
    },
  };

  await sendMail(message);
};

export default SQSHandler(eventHandler);
