variable "aws_account" {}

locals {
  root_path = "${path.module}/../.."
  packages_path = "${local.root_path}/packages"
  deps_filename = "${local.packages_path}/dependencies.zip"
  common_filename = "${local.packages_path}/common.zip"
  is_prod = terraform.workspace == "production"
}

module "swypeless-data" {
  source = "git@bitbucket.org:swypeless/swypeless-tf-modules.git//swypeless-data"

  aws_account = var.aws_account
  aws_region = var.aws_region
  env_name = terraform.workspace

  load_network = true
  load_events = true
}

data "template_file" "execution_policy" {
  template = file("${path.module}/policies/execution-policy.json")
  vars = {
    SERVICE_ENV = terraform.workspace
    AWS_ACCOUNT = var.aws_account
    AWS_REGION = var.aws_region
  }
}

locals {
  encoded_execution_policy = jsonencode(jsonencode(jsondecode(data.template_file.execution_policy.rendered)))
  execution_policy = trim(local.encoded_execution_policy, "\\\"")
}

data "template_file" "service" {
  template = file("${local.root_path}/service.json")
  vars = {
    INTERNAL_URL_ROOT = module.swypeless-data.network.route53-zone__swypeless-internal.name
    EXECUTION_POLICY = local.execution_policy
  }
}

locals {
  service_data = jsondecode(data.template_file.service.rendered)
  deps_name = "${local.service_data.name}__dependencies--${terraform.workspace}"
  common_name = "${local.service_data.name}__common--${terraform.workspace}"
}

resource "aws_lambda_layer_version" "deps" {
  filename = local.deps_filename
  layer_name = local.deps_name
  source_code_hash = filebase64sha256(local.deps_filename)
}

resource "aws_lambda_layer_version" "common" {
  filename = local.common_filename
  layer_name = local.common_name
  source_code_hash = filebase64sha256(local.common_filename)
}

module "service" {
  source = "git@bitbucket.org:swypeless/swypeless-tf-modules.git//lambda-async-microservice"

  aws_account = var.aws_account
  aws_region = var.aws_region

  service_name = local.service_data.name
  service_env = terraform.workspace

  source_sns_arn = module.swypeless-data.events.sns-topic__swypeless-events.arn

  functions = local.service_data.functions

  lambda_layers = [
    aws_lambda_layer_version.deps.arn,
    aws_lambda_layer_version.common.arn,
  ]

  packages_path = local.packages_path
}
